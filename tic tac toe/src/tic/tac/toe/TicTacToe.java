package tic.tac.toe;

import studijosKTU.ScreenKTU;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class TicTacToe extends ScreenKTU{
    
    private static final int gameSquareSize = 3;
    private static final int gameWindowSize = 200;
    
    private char playerMove = 'X';
    private int  movesAmount = 0;
    
    private char board[][] = new char[gameSquareSize][gameSquareSize];
    
    public TicTacToe(){
        super(gameWindowSize, gameWindowSize, gameSquareSize, gameSquareSize, 
              Grid.ON);
        setColors(Color.WHITE, Color.BLACK);
    }    
    
     @Override 
    public void mouseClicked(MouseEvent e) {
        
        int y = e.getY() / cellW;
        int x = e.getX() / cellH;
        
        if(board[x][y] != 'X' && board[x][y] != 'O' && isDone() != 'X' &&
           isDone() != 'O'){
            setColors(Color.WHITE, Color.BLACK);
            board[x][y] = playerMove;
            print(y, x, playerMove);
            
            movesAmount++;
            
            if(isDone() == playerMove){
                clearAll(Color.BLACK);
                setFontColor(Color.RED);
                print(0,1, playerMove);
                print(1,0,"WON");  
            }
        }
        
        if(playerMove == 'X')
            playerMove = 'O';
        else
            playerMove = 'X';
        
        if(movesAmount == 9 && isDone() != 'X' && isDone() != 'O'){
            clearAll(Color.BLACK);
            setFontColor(Color.RED);
            print(1, 0,"TIE");
        }
        
        refresh();
    }
    
    public char isDone(){
        if(board[0][0] == playerMove && board[0][1] == playerMove && board[0][2] == playerMove ||
           board[1][0] == playerMove && board[1][1] == playerMove && board[1][2] == playerMove ||
           board[2][0] == playerMove && board[2][1] == playerMove && board[2][2] == playerMove ||
           board[0][0] == playerMove && board[1][0] == playerMove && board[2][0] == playerMove ||
           board[0][1] == playerMove && board[1][1] == playerMove && board[2][1] == playerMove ||
           board[0][2] == playerMove && board[1][2] == playerMove && board[2][2] == playerMove ||
           board[0][0] == playerMove && board[1][1] == playerMove && board[2][2] == playerMove ||
           board[0][2] == playerMove && board[1][1] == playerMove && board[2][0] == playerMove){
            return playerMove;
        }
        
        return 0;
    }
    
    public static void main(String[] args) {
        TicTacToe game = new TicTacToe();
        game.refresh();
    }
}
