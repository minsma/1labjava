# 1 Duomenų Struktūrų su Java Labaratorinis darbas #

# Darbo eiga #
Duota: projektas Lab1A_BazinesStrukturos , kuriame yra pateiktos bazinių struktūrų demonstracinės klasės ir jų metodų testavimas ir projektas Lab1B_VizualiosStrukturos, kuriame demonstruojami vizualių objektų pavyzdžiai, panaudojant mokomąją klasę ScreenKTU.

1. Išsiaiškinti klasėse A_Labas, B_Labas, … , KlientuRinkinys pateiktus metodus, realizuoti  esančius tuščius metodus ir papildyti bent vieną klaę savais metodais. Atlikti jų testavimą.  
2. Sukurti kelis metodus pasirinktai teksto analizei (arba vienoje iš esančių D_, E_,... projekto klasių, arba sukurti projekte savo naują klasę (javos failą)).  
3. Sudaryti vieno iš pirmo kurso laboratorinių darbų programą Java kalba naujame NetBeans projekte.  
Duomenų įvedimas nėra būtinas – galima reikšmes priskirti arba generuoti atsitiktines.  
Projekte būtina: ne mažiau kaip du javos failai, masyvai, vienas paleidimo main metodas programos testavimui.  
Alternatyva gali būti uždavinio, pasirinkto iš toliau nurodytų rinkinių, sprendimas:  
LTU olimpiados - rekomenduojama nuo 2004 metų;  
Euler projektas - anglų kalba;  
Praktika su Java - anglų kalba;  
Įvairios programavimo problemos ir jų sprendimai.  
4. Atlikti klasių String ir ArrayList išeities kodų analizę - juos galima rasti http://grepcode.com/   
Gynimo metu atsakoma į klausimus apie šių klasių metodus.  
5. Išnagrinėti projektą Lab1B_VizualiosStrukturos. Galima sukurti naujus vizualius objektus ir išbandyti jų savybių keitimą. 
6. Sudaryti naują projektą, skirtą vizualių struktūrų apdorojimui. Galimos tematikos:  
- pirmo kurso uždaviniai su langeliais dvimatėje erdvėje;  
- ornamentų konstravimo eksperimentai, labirintai;  
- paprasti žaidimai: kryžiukai- nuliukai, gyvatėlė, sokoban, akis (su kortomis) ir pan.; 
- šaškių ir šachmatų etiudai, išdėstymas lentoje;  
- įvairių įvykių registravimas (pagal krepšinio tablo pavyzdį).  
