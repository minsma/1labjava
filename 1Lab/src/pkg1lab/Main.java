/** @author Mindaugas Smalinskas IFF-6/6
* 
* 11. Juvelyrikos parduotuvė. Turite UAB „Blizgučiai“ parduodamų žiedų sąrašą. Duomenų faile
pateikta informacija apie žiedus: gamintojas, pavadinimas, metalas, svoris, dydis, praba, kaina.
     Raskite brangiausią žiedą, ekrane atspausdinkite jo pavadinimą, metalą, skersmenį, svorį ir
    prabą.  
     Raskite, kokios prabos žiedų, parduotuvėje daugiausia, ekrane atspausdinkite prabą bei žiedų
    kiekį.
     Sudarykite balto aukso žiedų, pigesnių nei 300 eurų, sąrašą, į failą „BA300.csv“ įrašykite
    visus duomenis apie šiuos žiedus.
     Raskite 3 žiedus, kurių kaina tarp 300 ir 500 eurų, į failą įrašykite visus duomenis apie šiuos
    žiedus.
 */
package pkg1lab;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    
    // Maksimalus ziedu kiekis
    static final int MAX_RINGS_AMOUNT = 50;
    // Failas is kurio skaitoma
    static final String FILE_READ = "L1-11.csv";
    // Failas i kuri spausdinami balto aukso ziedai, kuriu kaina mazesne nei 300
    static final String FILE_WRITE1 = "BA300.csv";
    // Failas i kuri spausdinami visi like rezultatai
    static final String FILE_WRITE2 = "Results.csv";
    
    // Pradiniu ziedu masyvas
    static Ring[] rings;
    // Brangiausiu ziedu masyvas
    static Ring[] mostExpensiveRings;
    // Pigesniu nei 300 balto ausko ziedu masyvas
    static Ring[] cheaperThan300Rings;
    // 3 pirmu pasitaikiusiu ziedu tarp pateikto intervalo masyvas
    static Ring[] ringsBetweenInterval;
    // Prabu masyvas
    static int[] hallmarks;
    // Prabos pasikartojimo kiekio masyvas
    static int[] hallmarksCounters;
    // Pradiniu ziedu kiekis
    static int ringsCounter;
    // Brangiausiu ziedu kiekis
    static int mostExpensiveRingsCounter;
    // Prabu kiekis
    static int hallmarksCounter;
    // Pigesniu nei 300 balto ausko ziedu kiekis
    static int cheaperThan300RingsCounter;
    // Ziedu tarp duoto intervalo kiekis
    static int ringsBetweenIntervalCounter;
    // Pirmos duomenu failo eilutes masyvas
    static String[] fline;
    
    // Skaitomi duomenys is tekstinio failo
    static void ReadDataFromFile(){
        
        rings = new Ring[MAX_RINGS_AMOUNT];
        ringsCounter = 0;
        fline = new String[7]; 
        
        try {

            File f = new File(FILE_READ);

            BufferedReader b = new BufferedReader(new FileReader(f));

            String firstLine = b.readLine();
            fline = firstLine.split(";");
            
            String readLine = "";

            while ((readLine = b.readLine()) != null) {
                String[] values = readLine.split(";");
                String producer = values[0];
                String name = values[1];
                String metal = values[2];
                double weight = Double.parseDouble(values[3]);
                double size = Double.parseDouble(values[4]);
                int hallmark = Integer.parseInt(values[5]);
                double price = Double.parseDouble(values[6]);
                
                Ring ring = new Ring(producer, name, metal, weight,
                                     size, price, hallmark);
                
                rings[ringsCounter] = ring;
                ringsCounter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    // Surandama brangiausio ziedo kaina
    static double MostExpensiveRingPrice(Ring[] rings, int ringsCounter){
        double biggestPrice = 0;
        
        for(int i = 0; i < ringsCounter; i++){
            if(rings[i].GetPrice() > biggestPrice){
                biggestPrice = rings[i].GetPrice();
            }
        }
                
        return biggestPrice;
    }
    
    // Surandami ziedai, kuriu kaina lygi brangiausiam
    static void MostExpensiveRings(Ring[] rings, int ringsCounter, 
                                   double mostExpensiveRingPrice){
        mostExpensiveRings = new Ring[MAX_RINGS_AMOUNT];
        mostExpensiveRingsCounter = 0;
        
        for(int i = 0; i < ringsCounter; i++){
            if(rings[i].GetPrice() >= mostExpensiveRingPrice){
                mostExpensiveRings[mostExpensiveRingsCounter++] = rings[i];
            }
        }
    }
    
    // Surandama kiek skirtingu prabu yra ir kiek jos kartojasi
    static void DifferentHallmarksAndCounters(Ring[] rings, int ringsCounter){
        hallmarks = new int[MAX_RINGS_AMOUNT];
        hallmarksCounters = new int[MAX_RINGS_AMOUNT];
        hallmarksCounter = 0;
        
        for(int i = 0; i < ringsCounter; i++){
            if(DoesThisHallmarkExist(hallmarks, hallmarksCounter, 
                                     rings[i].GetHallmark()) == -1){
                hallmarks[hallmarksCounter] = rings[i].GetHallmark();
                hallmarksCounters[hallmarksCounter]++;
                hallmarksCounter++;
            } else{
                hallmarksCounters[DoesThisHallmarkExist(hallmarks,
                                                        hallmarksCounter,
                                                        rings[i].GetHallmark())]++;
            }          
        }
    }
    
    // Tikrinama ar tokia praba egzistuoja
    static int DoesThisHallmarkExist(int[] hallmarks, int hallmarksCounter, 
                                     int hallmark){
        for(int i = 0; i < hallmarksCounter; i++){
            if(hallmarks[i] == hallmark){
                return i;
            }
        }
        
        return -1;
    }
    
    // Surandama daugiausiai besikartojanti praba
    static int MostRecurHallmark(int[] hallmarks, int[] hallmarksCounters,
                                 int hallmarksCounter){
        int mostRecurHallmark = 0;
        int mostRecurHallmarkNumber = 0;
        
        for(int i = 0; i < hallmarksCounter; i++){
            if(hallmarksCounters[i] > mostRecurHallmark){
                mostRecurHallmark = hallmarksCounters[i];
                mostRecurHallmarkNumber = i;
            }
        }
        
        return mostRecurHallmarkNumber;
    }
    
    // Atrenkami balto aukso ziedai pigesni uz 300 
    static void CheaperThan300Rings(Ring[] rings, int ringsCounter){
        cheaperThan300Rings = new Ring[MAX_RINGS_AMOUNT];
        cheaperThan300RingsCounter = 0;
        
        for(int i = 0; i < ringsCounter; i++){
            if(rings[i].GetPrice() < 300.0 && 
               rings[i].GetMetal().equals("Baltas auksas")){
                cheaperThan300Rings[cheaperThan300RingsCounter++] = rings[i];
            }
        }
    }
    
    // Atrenkami 3 pirmi pasitaike ziedai tarp duoto intervalo
    static void ThreeRingsBetweenInterval(Ring[] rings, int ringsCounter){
        ringsBetweenInterval = new Ring[3];
        ringsBetweenIntervalCounter = 0;
        
        for(int i = 0; i < ringsCounter; i++){
            if(rings[i].GetPrice() >= 300 && rings[i].GetPrice() <= 500){
                if(ringsBetweenIntervalCounter < 3){
                    ringsBetweenInterval[ringsBetweenIntervalCounter++] = rings[i];
                } else{
                    break;
                }
            }
        }
    }
    
    // Atspausdinami i tekstini faila atrinkti ziedai
    static void PrintCheaperThan300Rings(Ring[] cheaperThan300Rings, 
                                         int cheaperThan300RingsCounter, 
                                         String[] fline){
        try{
            PrintWriter writer = new PrintWriter(FILE_WRITE1, "UTF-8");
            writer.println("-------------------------------------------------------------------------------------------");
            writer.printf("|%-21s|%-20s|%-20s|%-6s|%-6s|%-4s|%-6s|\n", fline[0], fline[1],
                    fline[2], fline[3], fline[4], fline[5], fline[6]);
            
            for(int i = 0; i < cheaperThan300RingsCounter; i++){
                writer.println("-------------------------------------------------------------------------------------------");
                writer.printf("|%-20s|%-20s|%-20s|%6s|%6s|%5s|%6s|\n", 
                        cheaperThan300Rings[i].GetProducer(), cheaperThan300Rings[i].GetName(), 
                        cheaperThan300Rings[i].GetMetal(), cheaperThan300Rings[i].GetWeight(), 
                        cheaperThan300Rings[i].GetSize(), cheaperThan300Rings[i].GetHallmark(), 
                        cheaperThan300Rings[i].GetPrice());
            }
            writer.println("-------------------------------------------------------------------------------------------");
            
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    // Atspausdinami i tekstini faila visi like rezultatai
    static void PrintResults(Ring[] rings, int ringsCounter, 
                             Ring[] mostExpensiveRings, int mostExpensiveCounter,
                             int mostRecurHallmarkNumber, int[] hallmarks,
                             int[] hallmarksCounters, Ring[] ringsBetweenInterval, 
                             int ringsBetweenIntervalCounter, String[] fline){
        try{
            PrintWriter writer = new PrintWriter(FILE_WRITE2, "UTF-8");
            writer.println("-------------------------------------------------------------------------------------------");
            writer.printf("|%-21s|%-20s|%-20s|%-6s|%-6s|%-4s|%-6s|\n", fline[0], fline[1],
                    fline[2], fline[3], fline[4], fline[5], fline[6]);
            
            for(int i = 0; i < ringsCounter; i++){
                writer.println("-------------------------------------------------------------------------------------------");
                writer.printf("|%-20s|%-20s|%-20s|%6s|%6s|%5s|%6s|\n", 
                        rings[i].GetProducer(), rings[i].GetName(), 
                        rings[i].GetMetal(), rings[i].GetWeight(), 
                        rings[i].GetSize(), rings[i].GetHallmark(), 
                        rings[i].GetPrice());
            }
            writer.println("-------------------------------------------------------------------------------------------");
            
            if (mostExpensiveCounter == 1){
                writer.println();
                writer.println("Brangiausias ziedas:");
                writer.println("------------------------------------------------------------------------");
                writer.printf("|%-20s|%-20s|%-10s|%-8s|%-8s|\n", fline[1], fline[2],
                    "Skersmuo", fline[3], fline[5]);
                writer.println("------------------------------------------------------------------------");
                writer.printf("|%-20s|%-20s|%10s|%8s|%8s|\n", 
                        mostExpensiveRings[0].GetName(), mostExpensiveRings[0].GetMetal(), 
                        mostExpensiveRings[0].GetSize(), mostExpensiveRings[0].GetWeight(), 
                        mostExpensiveRings[0].GetHallmark());
                writer.println("------------------------------------------------------------------------");
                } else{
                    writer.println("");
                    writer.println("Brangiausi ziedai:");
                    writer.println("------------------------------------------------------------------------");
                    writer.printf("|%-20s|%-20s|%-10s|%-8s|%-8s|\n", fline[1], fline[2],
                    "Skersmuo", fline[3], fline[5]);
                    writer.println("------------------------------------------------------------------------");
                    for (int i = 0; i < mostExpensiveCounter; i++)
                    {
                        writer.printf("|{0, -20}|{1, -20}|{2, 10}|{3, 9}|{4, 7}|\n", mostExpensiveRings[i].GetName(), 
                                mostExpensiveRings[i].GetMetal(), mostExpensiveRings[i].GetSize(), 
                                mostExpensiveRings[i].GetWeight(), mostExpensiveRings[i].GetHallmark());
                        writer.printf("|%-20s|%-20s|%10s|%8s|%8s|\n", 
                        mostExpensiveRings[0].GetName(), mostExpensiveRings[0].GetMetal(), 
                        mostExpensiveRings[0].GetSize(), mostExpensiveRings[0].GetWeight(), 
                        mostExpensiveRings[0].GetHallmark());
                        writer.println("------------------------------------------------------------------------");
                    }
                }
            
                writer.println("");
                writer.printf("Parduotuveje yra daugiausiai %s prabos ziedu, ju yra %s\n", 
                        hallmarks[mostRecurHallmarkNumber], hallmarksCounters[mostRecurHallmarkNumber]);
                writer.println("");

                writer.println("Trys ziedai tarp intervalo (300:500):");
                
                for (int i = 0; i < ringsBetweenIntervalCounter; i++)
                {
                    writer.println("-------------------------------------------------------------------------------------------");
                    writer.printf("|%-20s|%-20s|%-20s|%6s|%6s|%5s|%6s|\n", 
                        ringsBetweenInterval[i].GetProducer(), ringsBetweenInterval[i].GetName(), 
                        ringsBetweenInterval[i].GetMetal(), ringsBetweenInterval[i].GetWeight(), 
                        ringsBetweenInterval[i].GetSize(), ringsBetweenInterval[i].GetHallmark(), 
                        ringsBetweenInterval[i].GetPrice());
                }
                writer.println("-------------------------------------------------------------------------------------------");
            
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }        
    }
    public static void main(String[] args) {        
        ReadDataFromFile();        
        double mostExpensiveRingPrice = MostExpensiveRingPrice(rings, 
                                                               ringsCounter);
        MostExpensiveRings(rings, ringsCounter, mostExpensiveRingPrice);
        DifferentHallmarksAndCounters(rings, ringsCounter);
        int mostRecurHallmarkNumber = MostRecurHallmark(hallmarks, hallmarksCounters, hallmarksCounter);
        CheaperThan300Rings(rings, ringsCounter);
        ThreeRingsBetweenInterval(rings, ringsCounter);
        PrintCheaperThan300Rings(cheaperThan300Rings, cheaperThan300RingsCounter,
                                 fline);
        PrintResults(rings, ringsCounter, mostExpensiveRings, mostExpensiveRingsCounter,
                     mostRecurHallmarkNumber, hallmarks, hallmarksCounters,
                     ringsBetweenInterval, ringsBetweenIntervalCounter, fline);
    }
}
