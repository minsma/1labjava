/**
 * @author Mindaugas Smalinskas IFF-6/6
 */
package pkg1lab;

public class Ring {
    private String producer; // Gamintojas
    private String name;     // Pavadinimas
    private String metal;    // Metalas
    private double weight;   // Svoris
    private double size;     // Dydis
    private double price;    // Kaina
    private int hallmark;    // Praba
    
    // Tuscias konstruktorius
    public Ring(){}
    
    // Konstruktorius priskiriantis pradines reiksmes
    public Ring(String producer, String name, String metal, double weight, 
                double size, double price, int hallmark){
        this.producer = producer;
        this.name = name;
        this.metal = metal;
        this.weight = weight;
        this.size = size;
        this.price = price;
        this.hallmark = hallmark;
    }
    
    // Paimama gamintojo reiksme
    public String GetProducer(){
        return this.producer;
    }
    
    // Paimamas pavadinimo reiksme
    public String GetName(){
        return this.name;
    }
    
    // Paimama metalo reiksme
    public String GetMetal(){
        return this.metal;
    }
    
    // Paimama svorio reiksme
    public double GetWeight(){
        return this.weight;
    }
    
    // Paimama dydzio reiksme
    public double GetSize(){
        return this.size;
    }
    
    // Paimama kainos reiksme
    public double GetPrice(){
        return this.price;
    }
    
    // Paimama prabos reiksme
    public int GetHallmark(){
        return this.hallmark;
    }
}
